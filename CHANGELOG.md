# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2021-02-23
- Add a grey separator for the white on white case

## [0.2.0] - 2021-02-20
- Use "modern" python packaging files

## [0.1.1] - 2021-02-20
- Use django's css classname for inputs to have the same styling

## [0.1.0] - 2021-02-20
- Initial version.
